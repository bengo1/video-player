import React from 'react';
const PureRenderMixin = require('react-addons-pure-render-mixin');

import Video from './Video.jsx';
import Control from './Control.jsx';

export default React.createClass({
    mixins: [PureRenderMixin],
    getInitialState () {
        return {
            play: false,
            currentTime: 0
        };
    },
    togglePlay () {
        if (!this.state.play) {
            this.player.play()
        } else {
            this.player.pause();
        }
        this.setState({
            play: !this.state.play
        });
    },
    onTimeUpdate (currentTime) {
        this.setState({
            currentTime: currentTime
        });
    },
    onVideoLoaded (duration) {
        this.setState({
            duration: duration
        });
    },
    render () {
        let playerProps = {
            src: this.props.url,
            ref: (el) => {
                this.player = el;
            },
            onTimeUpdate: this.onTimeUpdate,
            onVideoLoaded: this.onVideoLoaded
        };
        // TODO add click event on control
        let controlProps = {
            togglePlay: this.togglePlay,
            play: this.state.play,
            ref: (el) => {
                this.control = el;
            },
            duration: this.state.duration,
            currentTime: this.state.currentTime
        };

        return <div className="container" ref="container">
            <Video {...playerProps}/>
            <Control {...controlProps} />
        </div>;
    }
});
