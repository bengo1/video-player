import React from 'react';
var PureRenderMixin = require('react-addons-pure-render-mixin');
const classNames = require('classnames');

export default React.createClass({
    mixins: [PureRenderMixin],
    render () {
        let bindClasses = classNames({
            'play-btn': true
        },
        {
            paused: this.props.play
        });
        return <div ref="control" className="control" >
            <div ref="play-btn" className={bindClasses} onClick={this.props.togglePlay}></div>
            <progress ref="progress" value={this.props.currentTime} max={this.props.duration}></progress>
        </div>;
    }
});
