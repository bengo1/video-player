import React from 'react';
const PureRenderMixin = require('react-addons-pure-render-mixin');

export default React.createClass({
    mixins: [PureRenderMixin],
    play () {
        this.refs.player.play();
        this.refs.player.addEventListener('timeupdate', this.update);
    },
    pause () {
        this.refs.player.pause();
        this.refs.player.removeEventListener('timeupdate', this.update);
        clearTimeout(this.time);
        this.time = null;
    },
    update () {
        if (!this.time) {
            this.time = setTimeout(() => {
                this.props.onTimeUpdate(this.refs.player.currentTime);
                this.time = null;
                this.update();
            }, 1000/24);
        }
    },
    componentDidMount () {
        this.refs.player.addEventListener('loadeddata', () => {
            this.props.onVideoLoaded(this.refs.player.duration);
        });
    },
    render () {
        return <video ref="player" src={this.props.src}>
        </video>;
    }
});
