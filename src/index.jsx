import React from 'react';
import ReactDOM from 'react-dom';
import VideoContainer from './components/Container';

require('./components/styles/style.scss');

const url = "https://s3-eu-west-1.amazonaws.com/onrewind-test-bucket/big_buck_bunny.mp4";

ReactDOM.render(
    <VideoContainer url={url} />,
    document.getElementById('app')
);
