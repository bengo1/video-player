import React from 'react';
import ReactDOM from 'react-dom';
import {
    renderIntoDocument,
    scryRenderedDOMComponentsWithTag
    } from 'react-addons-test-utils';
import {expect} from 'chai';

import Video from '../../src/components/Video.jsx';

describe('Tests are set up', () => {
    it('it works', () => {
        expect(true).to.be.ok;
    });
});

describe('Video components', () => {
    it('it renders', () => {
        const component = renderIntoDocument(
            <Video/>
        );

        const video = scryRenderedDOMComponentsWithTag(component, 'video');
        expect(video).to.be.ok;
    });

    it('the src attribute is set', () => {
        let playerProps = {
            src: 'test.mp4'
        };
        const component = renderIntoDocument(
            <Video {...playerProps}/>
        );

        const player = component.refs.player;
        expect(player.src).to.equal(playerProps.src);
    });

    it('the controls attribute is not set', () => {
        const component = renderIntoDocument(
            <Video/>
        );

        const player = component.refs.player;
        expect(player.controls).to.be.false;
    });
});
