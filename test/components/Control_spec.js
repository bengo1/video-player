import React from 'react';
import ReactDOM from 'react-dom';

import {
    renderIntoDocument,
    Simulate,
    scryRenderedDOMComponentsWithTag
    } from 'react-addons-test-utils';
import {expect} from 'chai';
import chai from 'chai';

import Control from '../../src/components/Control.jsx';


describe('Control components', () => {
    it('it renders', () => {
        let controlProps = {};

        const component = renderIntoDocument(
            <Control {...controlProps}/>
        );

        expect(component.refs['play-btn']).to.be.ok;
        expect(component.refs['progress']).to.be.ok;
    });

    it('properties are set', () => {
        const duration = 10;
        const currentTime = 5;
        let controlProps = {
            duration: duration,
            currentTime: currentTime
        };

        const component = renderIntoDocument(
            <Control {...controlProps}/>
        );
        expect(parseInt(component.refs.progress.getAttribute('max'))).to.equal(duration);
        expect(parseInt(component.refs.progress.getAttribute('value'))).to.equal(currentTime);
    });

    it('triggers callback property', () => {
        const togglePlay = function togglePlay () {};
        const spy = chai.spy(togglePlay);

        let controlProps = {
            togglePlay: spy,
        };
        const component = renderIntoDocument(
            <Control {...controlProps}/>
        );

        Simulate.click(ReactDOM.findDOMNode(component.refs['play-btn']));
        expect(spy).to.have.been.called();
    });
});

