import React from 'react';
import ReactDOM from 'react-dom';
import {
    renderIntoDocument,
    Simulate,
    scryRenderedDOMComponentsWithTag
    } from 'react-addons-test-utils';
import {expect} from 'chai';

import VideoContainer from '../../src/components/Container.jsx';


describe('Container components', () => {
    it('it renders with children', () => {
        const url = "";
        const component = renderIntoDocument(
            <VideoContainer url={url}/>
        );

        expect(component.refs.container).to.be.ok;

        const video = scryRenderedDOMComponentsWithTag(component, 'video');
        expect(video).to.be.ok;

    });
});

