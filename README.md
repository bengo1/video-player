# README #

### What is this repository for? ###

* Video Player build with React, Webpack, SASS. Unit tests covered with mocha/chai

### How do I get set up? ###

* Checkout the repository


```
#!command line

- cd <project_folder>

- npm install

```


### let's start ###

```
#!command line

- cd <project_folder>

- webpack-dev-server

```

* open Chrome at http://localhost:8080

### Running tests ###

```
#!command line

- cd <project_folder>

- npm run test

```